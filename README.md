# OpenML dataset: Meta_Album_PLK_Mini

https://www.openml.org/d/44282

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

## **Meta-Album Plankton Dataset (Mini)**
***
The Plankton dataset is created by researchers at the Woods Hole Oceanographic Institution (https://www.whoi.edu/). Imaging FlowCytobot (IFCB) was used for the data collection. The Complete process and mechanism are described in the paper [31]. Each image in the dataset contains one or multiple planktons. The images are captured in a controlled environment and have different orientations based on the flow of the fluid in which the images are captured and the size and shape of the planktons. The preprocessed plankton dataset is prepared from the original WHOI Plankton dataset. The preprocessing of the images is done by creating a background squared image by either duplicating the top and bottom-most 3 rows or the left and right most 3 columns based on the orientation of the original image to match the width or height of the image respectively. A Gaussian kernal of size 29x29 is applied to the background image to blur the image. Finally, the original plankton image is pasted on the background image at the center of the image. The squared background image with the original plankton image on top of it as one image is then resized into 128x128 with anti-aliasing.  



### **Dataset Details**
![](https://meta-album.github.io/assets/img/samples/PLK.png)

**Meta Album ID**: SM_AM.PLK  
**Meta Album URL**: [https://meta-album.github.io/datasets/PLK.html](https://meta-album.github.io/datasets/PLK.html)  
**Domain ID**: SM_AM  
**Domain Name**: Small Animals  
**Dataset ID**: PLK  
**Dataset Name**: Plankton  
**Short Description**: Plankton dataset from WHOI  
**\# Classes**: 86  
**\# Images**: 3440  
**Keywords**: plankton, ecology  
**Data Format**: images  
**Image size**: 128x128  

**License (original data release)**: MIT License  
**License URL(original data release)**: https://github.com/hsosik/WHOI-Plankton/blob/master/LICENSE
 
**License (Meta-Album data release)**: MIT License  
**License URL (Meta-Album data release)**: [https://github.com/hsosik/WHOI-Plankton/blob/master/LICENSE](https://github.com/hsosik/WHOI-Plankton/blob/master/LICENSE)  

**Source**: Woods Hole Oceanographic Institution  
**Source URL**: https://github.com/hsosik/WHOI-Plankton  
  
**Original Author**: Heidi M. Sosik, Emily E. Peacock, Emily F. Brownlee, Eric Orenstein  
**Original contact**: hsosik@whoi.edu  

**Meta Album author**: Ihsan Ullah  
**Created Date**: 01 March 2022  
**Contact Name**: Ihsan Ullah  
**Contact Email**: meta-album@chalearn.org  
**Contact URL**: [https://meta-album.github.io/](https://meta-album.github.io/)  



### **Cite this dataset**
```
@misc{whoiplankton,
      title={Annotated Plankton Images - Data Set for Developing and Evaluating Classification Methods.}, 
      author={Heidi M. Sosik, Emily E. Peacock, Emily F. Brownlee},
      year={2015},
      DOI = {10.1575/1912/7341},
      url={https://hdl.handle.net/10.1575/1912/7341}
}
```


### **Cite Meta-Album**
```
@inproceedings{meta-album-2022,
        title={Meta-Album: Multi-domain Meta-Dataset for Few-Shot Image Classification},
        author={Ullah, Ihsan and Carrion, Dustin and Escalera, Sergio and Guyon, Isabelle M and Huisman, Mike and Mohr, Felix and van Rijn, Jan N and Sun, Haozhe and Vanschoren, Joaquin and Vu, Phan Anh},
        booktitle={Thirty-sixth Conference on Neural Information Processing Systems Datasets and Benchmarks Track},
        url = {https://meta-album.github.io/},
        year = {2022}
    }
```


### **More**
For more information on the Meta-Album dataset, please see the [[NeurIPS 2022 paper]](https://meta-album.github.io/paper/Meta-Album.pdf)  
For details on the dataset preprocessing, please see the [[supplementary materials]](https://openreview.net/attachment?id=70_Wx-dON3q&name=supplementary_material)  
Supporting code can be found on our [[GitHub repo]](https://github.com/ihsaan-ullah/meta-album)  
Meta-Album on Papers with Code [[Meta-Album]](https://paperswithcode.com/dataset/meta-album)  



### **Other versions of this dataset**
[[Micro]](https://www.openml.org/d/44238)  [[Extended]](https://www.openml.org/d/44317)

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/44282) of an [OpenML dataset](https://www.openml.org/d/44282). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/44282/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/44282/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/44282/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

